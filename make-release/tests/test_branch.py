from unittest import mock
from unittest.mock import Mock

from mwrelease import branch
from requests.exceptions import HTTPError
import pytest


@mock.patch('mwrelease.branch.gerrit_client')
def test_delete(gerrit, capsys):
    gerrit.return_value.get.return_value = {'revision': '<revision>'}
    gerrit.return_value.put.return_value = {
        'object': '<revision>',
        'web_links': [
            {
                'url': 'http://example.org/sometag',
            }
        ]
    }
    branch.delete_branch('test/gerrit-ping', 'wmf/1.99.0-wmf.99')

    captured = capsys.readouterr()
    assert captured.out == (
        'Created http://example.org/sometag [<revision>]\n'
        'Deleting branch wmf/1.99.0-wmf.99 in test/gerrit-ping\n'
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_delete_noop_should_not_write(gerrit, capsys):
    gerrit.return_value.get.return_value = {'revision': '<revision>'}
    gerrit.return_value.put.side_effect = Exception
    gerrit.return_value.delete.side_effect = Exception
    branch.delete_branch('test/gerrit-ping', 'wmf/xx', noop=True)

    captured = capsys.readouterr()
    assert captured.out == (
        'Would create tag wmf/xx pointing to <revision>\n'
        'Would delete branch wmf/xx in test/gerrit-ping\n'
        )


@mock.patch('mwrelease.branch.gerrit_client')
def test_delete_tag_failure_does_not_delete_branch(gerrit, capsys):
    gerrit.return_value.get.return_value = {'revision': '<revision>'}
    gerrit.return_value.put.side_effect = Exception('<error>')
    delete = gerrit.return_value.delete

    with pytest.raises(Exception) as e:
        branch.delete_branch('test/gerrit-ping', 'wmf/xx')
    assert '<error>' == str(e.value)

    delete.assert_not_called()

    captured = capsys.readouterr()
    assert captured.out == (
            'Failed to create tag wmf/xx: <error>\n'
            'Aborting.\n'
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_create_branch_get_revision(gerrit, capsys):
    gerrit.return_value.put.return_value = {
        'revision': 'c0c703993ff8e52083943bec41628400939bc74e',
    }

    branch.create_branch('test/gerrit-ping', 'newbranch', 'HEAD')

    captured = capsys.readouterr()
    assert captured.out == (
        'Branching test/gerrit-ping to newbranch from HEAD: '
        'c0c703993ff8e52083943bec41628400939bc74e\n'
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_create_branch_handles_existing_branch(gerrit, capsys):
    Conflict = HTTPError(response=Mock(status_code=409))
    gerrit.return_value.put.side_effect = Conflict

    branch.create_branch('test/gerrit-ping', 'newbranch', 'START_HEAD')

    captured = capsys.readouterr()
    assert captured.out == (
        'Branching test/gerrit-ping to newbranch from START_HEAD\n'
        'Warning: Branch newbranch already exists in repository test/gerrit-ping\n'
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_create_branch_handles_API_error(gerrit, capsys):
    SomeError = HTTPError(
        'Some error',
        response=Mock(status_code=500, text='Internal server error')
    )
    gerrit.return_value.put.side_effect = SomeError

    with pytest.raises(HTTPError) as e:
        branch.create_branch('test/gerrit-ping', 'newbranch', 'START_HEAD')

    assert 'Some error' == str(e.value)
    captured = capsys.readouterr()
    assert captured.out == (
        'Branching test/gerrit-ping to newbranch from START_HEAD\n'
        'Failed to create branch newbranch: Internal server error\n'
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_delete_inexistent_branch_is_skipped(gerrit, capsys):
    NotFound = HTTPError(response=Mock(status_code=404))
    gerrit.return_value.get.side_effect = NotFound

    assert False is branch.delete_branch('test/gerrit-ping', 'inexistent/branch')

    captured = capsys.readouterr()
    assert captured.out == (
        "Repo test/gerrit-ping doesn't have a branch named inexistent/branch\n"
    )


@mock.patch('mwrelease.branch.gerrit_client')
def test_delete_error_getting_branch_raises_an_exception(gerrit, capsys):
    ServerError = HTTPError(response=Mock())
    gerrit.return_value.get.side_effect = ServerError

    with pytest.raises(HTTPError):
        branch.delete_branch('test/gerrit-ping', 'inexistent/branch')

        captured = capsys.readouterr()
        assert captured.out == ""


@mock.patch('subprocess.run')
def test_git_text_modes(run):
    branch.git()
    run.assert_called_with(mock.ANY, check=mock.ANY, universal_newlines=True)
    branch.git(universal_newlines=False)
    run.assert_called_with(mock.ANY, check=mock.ANY, universal_newlines=False)


@mock.patch('mwrelease.branch.gerrit_client')
@mock.patch('time.time')
def test_wait_for_change_to_merge(time, gerrit):
    gerrit.return_value.get.return_value = {'status': 'MERGED'}
    time.side_effect = [1, 40*60, 40*60, 40*60]  # start, timeout check, success_checkpoint, merged
    branch.wait_for_change_to_merge(12345)


@mock.patch('mwrelease.branch.gerrit_client')
@mock.patch('time.time')
def test_wait_for_change_waits(time, gerrit):
    gerrit.return_value.get.return_value = {'status': 'OPEN'}
    time.side_effect = [1, 40*60]  # start, timeout
    with mock.patch('time.sleep') as sleep:
        sleep.side_effect = Exception("Slept")

        with pytest.raises(Exception) as e:
            branch.wait_for_change_to_merge(12345)
            assert 'Slept' == str(e.value)
