#!/usr/bin/env python3

import argparse
import json
import logging
import os
import tempfile
import threading
import time

import app
import build_image_incr


# Maps MediaWiki image flavour names to build arguments.
MW_FLAVOURS = {
    "publish-81": {
        "PHP_VERSION": "8.1",
    },
    "publish": {
        "PHP_VERSION": "7.4",
    },
}

# Maps webserver image flavour names to build arguments.
WEB_FLAVOURS = {
    "webserver": {},
}


class App(app.App):
    def __init__(
        self,
        state_dir,
        staging_dir,
        full,
        http_proxy,
        https_proxy,
        **kwargs,
    ):
        super().__init__(**kwargs)

        self.state_dir = state_dir
        self.staging_dir = staging_dir
        self.full = full
        self.http_proxy = http_proxy
        self.https_proxy = https_proxy

    def docker_build(
        self,
        dockerfile,
        context_dir="empty",
        target=None,
        tag=None,
        pull=False,
        build_args=None,
        labels=None,
    ) -> str:
        """
        Builds an image using the specified `dockerfile`.

        Returns the output image id.
        """
        with tempfile.NamedTemporaryFile() as iidfile:
            cmd = [
                "docker",
                "build",
                "-f",
                dockerfile,
                "--iidfile",
                iidfile.name,
            ]
            if target:
                cmd += ["--target", target]
            if pull:
                cmd.append("--pull")
            if tag:
                cmd += ["--tag", tag]

            if self.http_proxy:
                cmd += ["--build-arg", f"http_proxy={self.http_proxy}"]
            if self.https_proxy:
                cmd += ["--build-arg", f"https_proxy={self.https_proxy}"]

            if build_args:
                for var, value in build_args.items():
                    if value:
                        cmd += ["--build-arg", f"{var}={value}"]

            if labels:
                for label in labels:
                    cmd += ["--label", label]

            cmd.append(context_dir)
            self.check_call(cmd)

            with open(iidfile.name) as i:
                image_id = i.readline().strip()

            return image_id

    def build_webserver_image(
        self,
        webserver_image_name,
        flavour,
        build_args,
        labels,
    ):
        """
        Returns the output image name, which might be a previously-built image if
        nothing changed since the last build.
        """
        self.logger.info("Building webserver-image-base")

        base_image_id = self.docker_build(
            "webserver/Dockerfile.webserver-base-image",
            pull=True,
            build_args=build_args,
        )

        tag = time.strftime("%Y-%m-%d-%H%M%S-webserver")
        fqin = f"{webserver_image_name}:{tag}"

        state_file = os.path.join(self.state_dir, f"webserver-{flavour}-state.json")
        report_file = os.path.join(self.state_dir, f"webserver-{flavour}-report.json")

        exclude = [
            "**/.git",
            "/wmf-config",
            "/tests",
            "/scap",
            "/src",
            "/multiversion",
            "/private",
            "/php-*",
        ]

        return build_image_incr.App(
            base_image_id,
            self.staging_dir,
            "/srv/mediawiki",
            fqin,
            exclude=exclude,
            state_file=state_file,
            report_file=report_file,
            full=self.full,
            push=True,
            labels=labels,
            logger=self.logger,
        ).run()

    def build_mediawiki_images(
        self,
        mediawiki_versions: list,
        mv_image_name,
        mv_debug_image_name,
        force_version,
        mediawiki_image_extra_packages,
        mediawiki_extra_ca_cert,
        flavour,
        build_args,
        labels,
    ):
        """
        Return the output image names of
        1) The multiversion image
        2) The multiversion debug image

        Returned image names may be names of previously-built images if nothing changed since the last build.
        """

        base = "single-version-base" if force_version else "multiversion-base"

        self.logger.info("Building %s", base)
        base_image_id = self.docker_build(
            "mediawiki-base/Dockerfile",
            context_dir="mediawiki-base",
            target=base,
            pull=True,
            build_args={
                **build_args,
                "MV_BASE_PACKAGES": mediawiki_image_extra_packages,
                "MV_EXTRA_CA_CERT": mediawiki_extra_ca_cert,
            },
        )

        include = []
        exclude = [".git", "**/cache/l10n/upstream", "/scap"]

        # Add --include's for active branches, based on reading the staging directory's
        # wikiversions.json file. If FORCE_MW_VERSION is set, use it as
        # the only active branch, ignoring wikiversions.json.

        if force_version:
            include.append(f"/php-{force_version}")
        else:
            for version in mediawiki_versions:
                include.append(f"/php-{version}")

        # And exclude any other trees that may be lurking
        exclude.append("/php-*")

        tag = time.strftime(f"%Y-%m-%d-%H%M%S-{flavour}")
        fqin = f"{mv_image_name}:{tag}"

        extra_commit_commands = []

        if force_version:
            extra_commit_commands.append(f"ENV FORCE_MW_VERSION={force_version}")

        version_type = "sv" if force_version else "mv"
        state_file = os.path.join(self.state_dir, f"mediawiki-{version_type}-{flavour}-state.json")
        report_file = os.path.join(self.state_dir, f"mediawiki-{version_type}-{flavour}-report.json")

        self.logger.info(
            "Building %s with MediaWiki versions %s",
            mv_image_name,
            ", ".join(mediawiki_versions),
        )

        image = build_image_incr.App(
            base_image_id,
            self.staging_dir,
            "/srv/mediawiki",
            fqin,
            include=include,
            exclude=exclude,
            extra_commit_commands=extra_commit_commands,
            state_file=state_file,
            report_file=report_file,
            full=self.full,
            push=True,
            labels=labels,
            logger=self.logger,
        ).run()
        debug_image = self.build_mediawiki_debug_image(
            mv_debug_image_name,
            tag,
            image,
            force_version,
            flavour,
            build_args,
            labels,
        )

        return image, debug_image

    def build_mediawiki_debug_image(
        self,
        mv_debug_image_name,
        tag,
        base,
        force_version,
        flavour,
        build_args,
        labels,
    ) -> str:
        """
        Returns the output image name, which might be a previously-built image if
        nothing changed since the last build.
        """
        self.logger.info("Building %s debug image", flavour)

        image_id = self.docker_build(
            "mediawiki-debug/Dockerfile",
            build_args={"BASE": base, **build_args},
            labels=labels,
        )

        state = {}
        version_type = "sv" if force_version else "mv"
        state_file = os.path.join(
            self.state_dir,
            f"mediawiki-debug-{version_type}-{flavour}-state.json",
        )
        if os.path.exists(state_file):
            with open(state_file) as f:
                state = json.load(f)

        if image_id == state.get("last_image_id"):
            mv_debug_image_name = state["last_image"]
            self.logger.info(
                "New debug image has the same image id as the prior, so using the old image %s",
                mv_debug_image_name,
            )
        else:
            mv_debug_image_name = f"{mv_debug_image_name}:{tag}"
            self.check_call(["docker", "tag", image_id, mv_debug_image_name])
            state["last_image"] = mv_debug_image_name
            state["last_image_id"] = image_id
            write_json_file(state, state_file)

        self.push_image(mv_debug_image_name)

        return mv_debug_image_name


def write_json_file(report, report_file):
    tmp_report_file = f"{report_file}.tmp"

    with open(tmp_report_file, "w") as f:
        json.dump(report, f, indent=4)
        f.write("\n")
    os.rename(tmp_report_file, report_file)


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s [%(name)s] %(message)s", datefmt="%H:%M:%S"
    )

    ap = argparse.ArgumentParser()

    ap.add_argument("state_dir")

    ap.add_argument(
        "--staging-dir",
        help="The host path of the MediaWiki staging directory",
        default="/srv/mediawiki-staging",
    )
    ap.add_argument(
        "--mediawiki-versions",
        help="A comma-separated list of MediaWiki versions to include in the image",
    )
    ap.add_argument(
        "--full",
        help="Disable incremental build",
        action="store_true",
    )
    ap.add_argument(
        "--multiversion-image-name",
        help="The name to use for the multiversion image, without tag",
        default="docker-registry.discovery.wmnet/restricted/mediawiki-multiversion",
    )
    ap.add_argument(
        "--multiversion-debug-image-name",
        help="The name to use for the multiversion debug image, without tag",
        default="docker-registry.discovery.wmnet/restricted/mediawiki-multiversion-debug",
    )
    ap.add_argument(
        "--webserver-image-name",
        help="The name to use for the webserver image, without tag",
        default="docker-registry.discovery.wmnet/restricted/mediawiki-webserver",
    )
    ap.add_argument(
        "--mediawiki-image-extra-packages",
        help="A space-separated list of extra packages to install in the mediawiki base image",
    )
    ap.add_argument(
        "--mediawiki-extra-ca-cert",
        help="A base64-encoded CA certificate to include in the mediawiki base image",
    )
    ap.add_argument(
        "--force-version",
        help="Create a single-version image using the specified version",
    )
    ap.add_argument(
        "--http-proxy", help="HTTP proxy to use", default=os.environ.get("http_proxy")
    )
    ap.add_argument(
        "--https-proxy",
        help="HTTPS proxy to use",
        default=os.environ.get("https_proxy"),
    )
    ap.add_argument(
        "--latest-tag",
        help="Tag to use for the latest image.",
        default="latest",
    )
    ap.add_argument(
        "--label",
        help="Label to add to built images.",
        action='append',
        default=[],
    )
    args = ap.parse_args()

    state_dir = args.state_dir

    staging_dir = args.staging_dir
    if not staging_dir.startswith("/"):
        raise SystemExit("The staging directory must be an absolute path")

    threaded_app = app.ThreadedApp(
        App,
        args=(
            state_dir,
            staging_dir,
            args.full,
            args.http_proxy,
            args.https_proxy,
        )
    )
    mutex = threading.Lock()
    mw_images_by_flavour = {}
    web_images_by_flavour = {}

    for flavour, build_args in MW_FLAVOURS.items():
        def build(app_instance, flavour, build_args):
            mw_mv_image, mw_mv_debug_image = app_instance.build_mediawiki_images(
                args.mediawiki_versions.split(","),
                args.multiversion_image_name,
                args.multiversion_debug_image_name,
                args.force_version,
                args.mediawiki_image_extra_packages,
                args.mediawiki_extra_ca_cert,
                flavour,
                build_args,
                args.label,
            )

            with mutex:
                mw_images_by_flavour[flavour] = {
                    "image": mw_mv_image,
                    "debug-image": mw_mv_debug_image,
                }
        threaded_app.run(
            f"mediawiki-{flavour}",
            target=build,
            args=(flavour, build_args),
        )

    for flavour, build_args in WEB_FLAVOURS.items():
        def build(app_instance, flavour, build_args):
            webserver_image = app_instance.build_webserver_image(
                args.webserver_image_name,
                flavour,
                build_args,
                args.label,
            )

            with mutex:
                web_images_by_flavour[flavour] = {
                    "image": webserver_image,
                }
        threaded_app.run(
            f"webserver-{flavour}",
            target=build,
            args=(flavour, build_args),
        )

    threaded_app.join()

    latest_mw_image = mw_images_by_flavour["publish"]["image"]
    latest_debug_image = mw_images_by_flavour["publish"]["debug-image"]
    latest_web_image = web_images_by_flavour["webserver"]["image"]

    # Update the latest tag for all images
    for image in [latest_mw_image, latest_debug_image, latest_web_image]:
        def tag_latest(app_instance, image):
            app_instance.push_image(image, tag=args.latest_tag)

        threaded_app.run("tag-latest", target=tag_latest, args=(image, ))

    threaded_app.join()

    # TODO remove the redundant entries below after flavours are supported by
    # all scap deployments
    report = {
        "mediawiki": {
            "multiversion-image": latest_mw_image,
            "multiversion-debug-image": latest_debug_image,
            "by-flavour": mw_images_by_flavour,
        },
        "webserver": {
            "image": latest_web_image,
            "by-flavour": web_images_by_flavour,
        },
    }

    write_json_file(report, os.path.join(state_dir, "report.json"))


if __name__ == "__main__":
    main()
